package ru.t1.stepanishchev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

}
