package ru.t1.stepanishchev.tm.api.service;

import ru.t1.stepanishchev.tm.api.repository.IRepository;
import ru.t1.stepanishchev.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}