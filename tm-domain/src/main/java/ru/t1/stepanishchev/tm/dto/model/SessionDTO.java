package ru.t1.stepanishchev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public final class SessionDTO extends AbstractUserOwnedModelDTO implements Cloneable{

    @Nullable
    @Column
    private String signature;

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @NotNull
    @Column(name = "date")
    private Date date = new Date();

    public SessionDTO(@NotNull String userId) {
        this.userId = userId;
    }

    @Nullable
    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = true)
    private Role role = null;

}
