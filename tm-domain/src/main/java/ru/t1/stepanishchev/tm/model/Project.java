package ru.t1.stepanishchev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.model.IWBS;
import ru.t1.stepanishchev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    @Column(name = "name", nullable = false, length = 50)
    private String name = "";

    @Nullable
    @Column(name = "description", nullable = true, length = 200)
    private String description = "";

    @NotNull
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @OneToMany(mappedBy = "project")
    private List<Task> tasks = new ArrayList<>();

    public Project(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}