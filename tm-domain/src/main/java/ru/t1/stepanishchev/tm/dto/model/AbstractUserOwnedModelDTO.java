package ru.t1.stepanishchev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModelDTO extends AbstractModelDTO {

    @NotNull
    @Column(nullable = false, name = "user_id")
    private String userId;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name = "";

    @NotNull
    @Column(name = "description", nullable = false)
    private String description = "";

    @NotNull
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "created", nullable = false)
    private Date created = new Date();

    @Nullable
    @Column(name = "date_begin")
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    private Date dateEnd;

    public AbstractUserOwnedModelDTO (
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final Date dateBegin
    ) {
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
    }

    @Override
    public String toString() {
        return name + "; " + description + "; " + dateBegin + ". ";
    }

}
